from django.contrib import admin
from .models.Category import Category
from .models.Comments import Comments
from .models.Interest import Interest
from .models.Post import Post
from .models.Preferences import Preferences
from .models.User import User

admin.site.register(User)
admin.site.register(Category)
admin.site.register(Preferences)
admin.site.register(Post)
admin.site.register(Comments)
admin.site.register(Interest)
# Register your models here.
