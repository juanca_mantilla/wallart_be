from django.db import models
from .User import User
from .Post import Post

class Interest(models.Model):
    idInterest = models.AutoField(primary_key = True)
    idUserFK = models.ForeignKey(User ,related_name= 'Interest_idUserFK' , on_delete=models.CASCADE) 
    idPostFK = models.ForeignKey(Post ,related_name= 'interest_idPostFK' , on_delete=models.CASCADE)
    interestDate = models.DateField()


